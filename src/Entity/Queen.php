<?php

namespace App\Entity;

class Queen extends Bees
{
    public $hp = 100;

    public $damages = 15;

    public $status = "alive";

    public $label = "queen";

    public function hitAction($hp, $damages)
    {
        $this->hp = $this->hp - $this->damages;
    }

    /**
     * @param int $hp
     */
    public function setHp(int $hp): void
    {
        $this->hp = $hp;
    }

    /**
     * @return int
     */
    public function getHp(): int
    {
        return $this->hp;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }
}