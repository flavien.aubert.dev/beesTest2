<?php

namespace App\Entity;

class Scout extends Bees
{
    public $hp = 30;
    public $damages = 15;
    public $status = "alive";

    public $label = "scout";

    public function hitAction($hp, $damages)
    {
        $this->hp = $this->hp - $this->damages;
    }

    /**
     * @return int
     */
    public function getHp(): int
    {
        return $this->hp;
    }

    /**
     * @param int $hp
     */
    public function setHp(int $hp): void
    {
        $this->hp = $hp;
    }

    /**
     * @return int
     */
    public function getDamages(): int
    {
        return $this->damages;
    }

    /**
     * @param int $damages
     */
    public function setDamages(int $damages): void
    {
        $this->damages = $damages;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }
}