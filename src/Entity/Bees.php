<?php

namespace App\Entity;

abstract class Bees
{
    protected $hp;
    protected $damages;
    protected $status;

    protected $label;

    abstract public function hitAction($hp, $damages);
}