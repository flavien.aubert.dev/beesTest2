<?php

namespace App\Entity;

class Worker extends Bees
{
    public $hp = 50;
    public $damages = 20;
    public $status = "alive";
    public $label = "worker";

    public function hitAction($hp, $damages)
    {
        $this->hp = $this->hp - $this->damages;
    }
}