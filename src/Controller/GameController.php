<?php

namespace App\Controller;

use App\Manager\GameManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
class GameController extends AbstractController
{
    private GameManager $gameManager;
    public function __construct(GameManager $gameManager)
    {
        $this->gameManager = $gameManager;
    }

    #[Route('/', name: 'app_game')]
    public function index(): Response
    {
       $game = $this->gameManager->initGame();
        return $this->render('game\game.html.twig',[
            'game' => $game
        ]);
    }

    #[Route('/hit', name: 'app_hit')]
    public function hitAction(Request $request){
        if($request->isXmlHttpRequest())
        {
            dd($request->request->all());

            //todo: decrementation HP de la case selectionnée avec les dommages definis pour celle-ci
        }
    }
}
