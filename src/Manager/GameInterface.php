<?php

namespace App\Manager;

interface GameInterface
{
    const nbWorker = 5;
    const nbScout = 8;
    const nbQueen = 1;
    const maxBees = 14;

    public function initGame();
}