<?php
namespace App\Manager;

use App\Entity\Queen;
use App\Entity\Scout;
use App\Entity\Worker;

class GameManager implements GameInterface
{
    public function initGame(): array
    {
        $bees = [];

        $bees[] = new Queen();
        for ($w = 0; $w <= self::nbWorker; $w++)
        {
            $bees[] = new Worker();
        }

        for ($s = 0; $s <= self::nbScout; $s++)
        {
            $bees[] = new Scout();
        }

        return $bees;
    }
}